import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * Created by wladislaw on 05.09.16.
 */
class MyData {
    private Integer itn;
    private String str;

    public void setItn(Integer itn) {
        this.itn = itn;
    }

    public void setStr(String str) {
        this.str = str;
    }

    public Integer getItn() {
        return itn;
    }

    public String getStr() {
        return str;
    }

    public MyData(Integer itn, String str) {
        this.itn = itn;
        this.str = str;
    }

    @Override
    public String toString() {
        return "MyData{" +
                "itn=" + itn +
                ", str='" + str + '\'' +
                '}';
    }
}

public class Data {
    List<MyData> data = new ArrayList();
    Random rand = new Random();

    public Data() {
        for (int i = 0; i < 100; i++) {
            MyData data = new MyData(rand.nextInt(), Boolean.toString(rand.nextBoolean()));
            this.data.add(data);
        }
    }

    public static void main(String[] args) {
        Data dataObj = new Data();
        for (int i = 0; i <dataObj.data.size() ; i++) {
            if(dataObj.dataChecker(dataObj.data.get(i), new RuleTwo ()))
                System.out.println(dataObj.data.get(i)+" "+i+1);
        }

    }
    public boolean dataChecker(MyData myData, Rule rule){
        return rule.ruleFunk(myData);
    }

    public void dataController(MyData myFata, int itn, String str) {
        if (!myFata.equals(null)) {
            myFata.setItn(itn);
            myFata.setStr(str);
        } else throw new NullPointerException();
    }
}
