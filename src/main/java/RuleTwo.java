/**
 * Created by wladislaw on 05.09.16.
 */
public class RuleTwo implements Rule {

    @Override
    public boolean ruleFunk(MyData myData) {
        return myData.getStr().equals("false") &&
                myData.getItn() %2 == 0 ?true: false;
    }
}
